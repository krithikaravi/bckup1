﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;

namespace API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            //Enable CORS

             config.EnableCors(new EnableCorsAttribute("http://localhost:4200", headers: "*", methods: "*"));
            // config.EnableCors();
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "defaultapi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //    config.Routes.MapHttpRoute(
            //    name: "EventDetails",
            //    routeTemplate: "api/{controller}/{action}/{associateID}/{favEvent}",
            //    defaults: new { action = "FavEvents" }
            //);

            config.Routes.MapHttpRoute(
                name: "DefaultApi1",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));


           // config.MapHttpAttributeRoutes();
         //   config.MessageHandlers.Add(new AuthenticationHandler());
           // config.Routes.MapHttpRoute(
           // name: "DefaultApi",
           // routeTemplate: "api/{controller}/{id}",
           // defaults: new
           // {
           //     id = RouteParameter.Optional
           // });

           // config.Routes.MapHttpRoute(
           //name: "DefaultApi1",
           //routeTemplate: "api/{controller}/{action}/{id}",
           //defaults: new { id = RouteParameter.Optional, Action = RouteParameter.Optional });


            //To produce JSON format add this line of code  
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }
    }
}
