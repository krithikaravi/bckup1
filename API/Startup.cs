﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using API.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Infrastructure;

[assembly: OwinStartup(typeof(API.Startup))]
namespace API
{
    public class Startup
    {
        //public void Configuration(IAppBuilder app)
        //{
        //    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
        //    //app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

        //    //var provider = new AuthorisationServerProvider();
        //    //OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions
        //    //{
        //    //    AllowInsecureHttp=true,
        //    //    TokenEndpointPath=new PathString("/token"),
        //    //    AccessTokenExpireTimeSpan=TimeSpan.FromDays(1),
        //    //    Provider=provider
        //    //};
        //    //app.UseOAuthAuthorizationServer(options);
        //    //app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

        //    //HttpConfiguration config = new HttpConfiguration();
        //    //WebApiConfig.Register(config);
        //}

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            // GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        private void ConfigureAuth(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            var OAuthOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/Token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(20),
                Provider = new AuthorizationServerProvider()
            

    //TokenEndpointPath = new PathString("/Token"),
    //Provider = new ApplicationOAuthProvider(),
    //AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
    //AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
    ////ONLY FOR DEVELOPING: ALLOW INSECURE HTTP!
    //AllowInsecureHttp = true
        };
            app.UseOAuthBearerTokens(OAuthOptions);
            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
        }


    }
}
