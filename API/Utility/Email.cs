﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace API.Utility
{
    internal static class Email
    {
        public static void SendEmail(string toAddress, string ccList, string subject, string mailBody)
        {
            string fromAddress = Convert.ToString(ConfigurationManager.AppSettings["FromAddress"]);
            string smtpServer = Convert.ToString(ConfigurationManager.AppSettings["SMTPServer"]);
            int smtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);

            MailMessage message = new MailMessage();
            SmtpClient client = new SmtpClient();
            try
            {
                MailAddress from = new MailAddress(fromAddress);
                message.From = from;

                if (toAddress.Contains(";"))
                {
                    string[] arrToList = toAddress.Split(';');
                    foreach (var to in arrToList)
                    {
                        message.To.Add(to);
                    }
                }
                else
                    message.To.Add(toAddress);

                if (!string.IsNullOrEmpty(ccList))
                    message.CC.Add(ccList);
                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = mailBody;
                client.Host = smtpServer;
                client.Port = smtpPort;
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new System.Net.NetworkCredential(from.Address, "123Welcome$");
                client.Send(message);
            }
            catch (Exception e)
            {

            }
        }
    }
}