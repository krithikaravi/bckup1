﻿using API.Utility;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Http;

namespace API.Functions
{
    public class VolunteerFunctions
    {
        private MyDbContext db = new MyDbContext();

        // GET: api/AssociateDetails
        public IQueryable<UserRoles> GetUserRoles()
        {

            //var  data = (from ur in db.UserRoles
            //             join emp in db.AssociateDetails on ur.EmployeeID equals emp.EmployeeID

            //             select new 
            //             {

            //                 EmployeeID = ur.EmployeeID,
            //                 RoleId = ur.RoleId,
            //                 Roles = (from r in db.Roles where r.RoleId == ur.RoleId select r).ToList(),
            //                 Associates = (from a in db.AssociateDetails where ur.EmployeeID == a.EmployeeID select a).ToList()

            //             }).ToList().Select(x => new UserRoles()
            //             {
            //                 EmployeeID = x.EmployeeID,
            //                 RoleId = x.RoleId,
            //                 Roles = x.Roles,
            //                 Associates = x.Associates

            //             }).ToList().AsQueryable();

            var data = (from ad in db.AssociateDetails
                        join ur in db.UserRoles on ad.EmployeeID equals ur.EmployeeID
                        join r in db.Roles on ur.RoleId equals r.RoleId
                        select new
                        {
                            Id = ur.Id,
                            EmployeeID = ur.EmployeeID,
                            RoleId = ur.RoleId,
                            RoleType = ur.RoleType
                            //Roles = (from ro in db.Roles where ro.RoleId == ur.RoleId select ro).ToList(),
                            //AssociateName = (from emp in db.AssociateDetails where emp.EmployeeID==ur.EmployeeID select new AssociateDetails { AssociateName=emp.AssociateName }).ToList().FirstOrDefault(),
                        }).ToList()
            .Select(x => new UserRoles { Id = x.Id, EmployeeID = x.EmployeeID, RoleId = x.RoleId, RoleType = x.RoleType }).ToList().AsQueryable();
            return data;


        }
        // GET: api/AssociateDetails
        public IQueryable<AssociateRoles> GetAssociateRoles()
        {
            //            var data = (from ad in db.AssociateDetails
            //                        join
            //ur in db.UserRoles on ad.EmployeeID equals ur.EmployeeID
            //                        select new AssociateRoles
            //                        {
            //                            AssociateId = ad.EmployeeID,
            //                            AssociateName = ad.AssociateName,
            //                            ContactNumber = ad.ContactNumber,
            //                            EmailId = ad.EmailId,
            //                            RoleType = ur.RoleType
            //                        });

            //            return data;

            var data=(from ad in db.AssociateDetails
            join ur in db.UserRoles on ad.EmployeeID equals ur.EmployeeID into ps
            from p in ps.DefaultIfEmpty()
            select new AssociateRoles{
                AssociateId = ad.EmployeeID,
                AssociateName = ad.AssociateName,
                ContactNumber = ad.ContactNumber,
                EmailId = ad.EmailId,
                RoleType = p == null ? "" : p.RoleType });
            return data;
        }

        public IQueryable<AssociateRoles> GetAssociateRoles(string email, string password)
        {
            //            var data = (from ad in db.AssociateDetails
            //                        join
            //ur in db.UserRoles on ad.RoleId equals ur.RoleId
            //                        select new AssociateRoles
            //                        {
            //                            AssociateId = ad.EmployeeID,
            //                            AssociateName = ad.AssociateName,
            //                            ContactNumber = ad.ContactNumber,
            //                            EmailId = ad.EmailId,
            //                            RoleType = ur.RoleType
            //                        });
            //            return data;
            //var data = (from ad in db.AssociateDetails
            //            where ad.EmailId == email && ad.Password == password
            //            select new AssociateRoles
            //            {
            //                AssociateId = ad.EmployeeID,
            //                AssociateName = ad.AssociateName,
            //                ContactNumber = ad.ContactNumber,
            //                EmailId = ad.EmailId,
            //            });
            //return data;
            var data = (from ad in db.AssociateDetails
                        join ur in db.UserRoles on ad.EmployeeID equals ur.EmployeeID
                        join r in db.Roles on ur.RoleId equals r.RoleId
                        where ad.EmailId == email && ad.Password == password
                        select new AssociateRoles
                        {
                            AssociateId = ur.EmployeeID,
                            AssociateName = ad.AssociateName,
                            ContactNumber = ad.ContactNumber,
                            EmailId = ad.EmailId,
                            RoleId = ur.RoleId,
                            RoleType = r.RoleType
                        });
            return data;
        }

        public void NotifyEventRejectedToPOC(EventDetails eventDetails)
        {
            if (eventDetails.PocName == null)
            {

                eventDetails.PocName =( from ed in db.EventDetails
                                     join ad in db.AssociateDetails
                                     on ed.PocId equals ad.EmployeeID
                                     where ed.EventId == eventDetails.EventId
                                     select ad).FirstOrDefault();

                    
            }
            string emailBody = GetEventRejectedEmailBody(eventDetails);
            string toAddress = eventDetails.PocName.EmailId;
            Email.SendEmail(toAddress, string.Empty, "Event Rejected by Outreach Team", emailBody);
        }

        public IQueryable<AssociateDetails> GetAssociateDetailsByEventId(int eventId)
        {
            var data = (from ad in db.AssociateDetails
                        join
                        er in db.EventRegistrations on ad.Id equals er.AssociateDetailsId
                        where er.EventId == eventId
                        select ad);

            return data;
        }
        public int GetVolunteerHrsbyEmpid(int empid)
        {
            var endTime = db.EventDetails.FirstOrDefault();
            var hours = (endTime.EndTime - endTime.StartTime).Hours;
            return hours;
        }

        public IQueryable<AssociateRoles> GetAssociateRole(string email)
        {
            var data = (from ad in db.AssociateDetails
                        join ur in db.UserRoles on ad.EmployeeID equals ur.EmployeeID
                        join r in db.Roles on ur.RoleId equals r.RoleId
                        where ad.EmailId == email
                        select new AssociateRoles
                        {
                            AssociateId = ur.EmployeeID,
                            AssociateName = ad.AssociateName,
                            ContactNumber = ad.ContactNumber,
                            EmailId = ad.EmailId,
                            RoleId = ur.RoleId,
                            RoleType = r.RoleType
                        });
            return data;
        }

        //public IQueryable<AssociateRoles> GetAssociateRolesByID(int id)
        //{
        //    //var data = (from ad in db.AssociateDetails
        //    //            join ur in db.UserRoles on ad.RoleId equals ur.RoleId
        //    //            where ur.RoleId == id
        //    //            select new AssociateRoles
        //    //            {
        //    //                AssociateId = ad.Id,
        //    //                AssociateName = ad.AssociateName,
        //    //                //  ContactNumber = ad.ContactNumber,
        //    //                EmailId = ad.EmailId,
        //    //                RoleType = ur.RoleType
        //    //            });
        //    //return data;
        //}

        public void CreateAssociateDetails(AssociateDetails associateDetails)
        {
            if (associateDetails == null)
            {
                throw new NotImplementedException("Not initialized");
            }
            else
            {
                //associateDetails.AssociateName = "New NAME";
                //// associateDetails.ContactNumber = "12345";
                //associateDetails.EmailId = "new mail id";
                //db.AssociateDetails.Add(associateDetails);
                //db.SaveChanges();
                // associateDetails.RoleId = 1;
                db.AssociateDetails.Add(associateDetails);
                db.SaveChanges();
            }
        }
        public void BulkUplaodEventDetails(List<EventDetails> eventDetails)
        {
            if (eventDetails == null)
            {
                throw new NotImplementedException("Not initialized");
            }
            else
            {
                foreach (var events in eventDetails)
                {
                    //events.TransportTypeId = 1;

                    var eventCode = (from ed in db.EventDetails
                                     orderby ed.EventId descending
                                     select new { code = ed.EventCode }).ToList().
                        Select(x => new EventDetails()
                        {
                            EventCode = x.code
                        });
                    if (eventCode.ToList().Count == 0)
                    { events.EventCode = "EVNT" + 00010000 + ""; }
                    else
                    {
                        string eCode = eventCode.ToList()[0].EventCode.Substring(4);
                        long newEventCode = Convert.ToInt64(eCode) + 1;
                        events.EventCode = "EVNT" + newEventCode;
                    }
                    db.EventDetails.Add(events);
                    string emailBody = GetEventCreationEmaiBody(events);
                    string pmoEmail = GetPMODetails();
                    Email.SendEmail(pmoEmail, string.Empty, "Event Created Succesfully", emailBody);
                }

                db.SaveChanges();
            }
        }

        public bool EditAssociateDetails(AssociateDetails associateDetails)
        {
            db.Entry(associateDetails).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }

        public void DeleteAssociateDetails(int id, AssociateDetails associateDetails)
        {
            var details = db.AssociateDetails.Where(t => t.AssociateName == associateDetails.AssociateName).ToList();
            if (details.Count > 0)
            {
                for (int i = 0; i < details.Count; i++)
                {
                    db.AssociateDetails.Remove(details[i]);
                }
                db.SaveChanges();
            }

        }



        public void CreateEventDetails(EventDetails eventDetails)
        {
            if (eventDetails == null)
            {
                throw new NotImplementedException("Not initialized");
            }
            else
            {
                //eventDetails.EventStatusId = 2;
                //eventDetails.TransportTypeId = 1; 
                //"EVNT" + 00010000;
                var eventCode = (from ed in db.EventDetails
                                 orderby ed.EventId descending
                                 select new { code = ed.EventCode }).ToList().
                    Select(x => new EventDetails()
                    {
                        EventCode = x.code
                    });
                if (eventCode.ToList().Count == 0)
                { eventDetails.EventCode = "EVNT" + 00010000 + ""; }
                else
                {
                    string eCode = eventCode.ToList()[0].EventCode.Substring(4);
                    long newEventCode = Convert.ToInt64(eCode) + 1;
                    eventDetails.EventCode = "EVNT" + newEventCode;
                }
                db.EventDetails.Add(eventDetails);
                db.SaveChanges();
            }
            string emailBody = GetEventCreationEmaiBody(eventDetails);
            string pmoEmail = GetPMODetails();
            Email.SendEmail(pmoEmail, string.Empty, "Event Created Succesfully", emailBody);
        }
        public string GetPMODetails()
        {
            var associateDetails = from r in db.Roles
                                   join ur in db.UserRoles
                                   on r.RoleId equals ur.RoleId
                                   join ad in db.AssociateDetails
                                   on ur.EmployeeID equals ad.EmployeeID
                                   where r.RoleType.ToLower() == "pmo"
                                   select ad;
            return string.Join(",", associateDetails.Select(r => r.EmailId));
        }
        public IQueryable<ViewEventRegistrations> GetRegistrationDetail()
        {
            var data = (from er in db.EventRegistrations
                        join
                        ed in db.EventDetails on er.EventId equals ed.EventId
                        join
ad in db.AssociateDetails on er.EmployeeID equals ad.EmployeeID
                        where er.ParticipationId == 1
                        select new ViewEventRegistrations
                        {
                            Id = er.Id,
                            EventCode = ed.EventCode,
                            EventName = ed.EventName,
                            EventDescription = ed.EventDescription,
                            EventDate = ed.EventDate,
                            StartTime = ed.StartTime,
                            LocationName = ed.LocationName,
                            EndTime = ed.EndTime,
                            AssociateDetailsId = er.AssociateDetailsId,
                            BusinessUnitId = er.BusinessUnitId,
                            ParticipationId = er.ParticipationId,
                            WaitingList = er.WaitingList,
                            EventId = ed.EventId,
                            EmployeeID = ad.EmployeeID,
                            EmployeeName = ad.AssociateName,
                            ContactNumber = ad.ContactNumber,
                            Email = ad.EmailId
                        });
            return data;
        }

        public void SendUnregisterNotification(EventRegistrations eventRegistrations)
        {
            string emailBody = GetUnregisterEmailBody(eventRegistrations);
            string emailSubject = string.Format("Outreach Notification for you: Unregister for {0}", eventRegistrations.Event.EventName);
            Email.SendEmail(eventRegistrations.EmailId, string.Empty, emailSubject, emailBody);
        }

        public IQueryable<ViewEventRegistrations> GetRegistrationDetails(int id)
        {
            var data = (from er in db.EventRegistrations.Include(r => r.ParticipationDetails)
                        join
                        ed in db.EventDetails on er.EventId equals ed.EventId
                        join
                        ad in db.AssociateDetails on er.EmployeeID equals ad.EmployeeID
                        //where er.ParticipationId == 1
                        where er.ParticipationDetails.ParticipationStatus.ToLower() == "registered"
                       // && ed.EventId == id
                        select new ViewEventRegistrations
                        {
                            Id = er.Id,
                            EventCode = ed.EventCode,
                            EventName = ed.EventName,
                            EventDescription = ed.EventDescription,
                            EventDate = ed.EventDate,
                            StartTime = ed.StartTime,
                            LocationName = ed.LocationName,
                            EndTime = ed.EndTime,
                            AssociateDetailsId = er.AssociateDetailsId,
                            BusinessUnitId = er.BusinessUnitId,
                            ParticipationId = er.ParticipationId,
                            WaitingList = er.WaitingList,
                            EventId = ed.EventId,
                            EmployeeID = ad.EmployeeID,
                            EmployeeName = ad.AssociateName,
                            ContactNumber = ad.ContactNumber,
                            Email = ad.EmailId
                        });
            return data;
        }

        public IQueryable<ViewEventDetails> GetEventDetails()
        {
            //            var data = (from ed in db.EventDetails
            //                        join
            //es in db.EventStatus on ed.EventStatusId equals es.Id
            //                        join
            //t in db.Transports on ed.TransportTypeId equals t.Id
            //                        join
            //ad in db.AssociateDetails on ed.PocId equals ad.EmployeeID
            //                        select new ViewEventDetails
            //                        {
            //                            EventId = ed.EventId, 
            //                            EventCode = ed.EventCode,
            //                            LocationName = ed.LocationName,
            //                            BeneficiaryName = ed.BeneficiaryName,
            //                            CouncilName = ed.CouncilName,
            //                            EventName = ed.EventName,
            //                            EventDescription = ed.EventDescription,
            //                            EventDate = ed.EventDate,
            //                            EmployeeID = ed.PocId,
            //                            EmployeeName = ad.AssociateName,
            //                            StartTime = ed.StartTime,
            //                            EndTime = ed.EndTime,
            //                            Address=ed.Address
            //                        });
            //            return data;
            int count = db.EventDetails.Count();
            var data = (from ed in db.EventDetails
                        join
t in db.Transports on ed.TransportTypeId equals t.Id
                        join
ad in db.AssociateDetails on ed.PocId equals ad.EmployeeID
orderby ed.EventId ascending
                        select new ViewEventDetails
                        {
                            EventId = ed.EventId,
                            EventCode = ed.EventCode,
                            LocationName = ed.LocationName,
                            BeneficiaryName = ed.BeneficiaryName,
                            CouncilName = ed.CouncilName,
                            EventName = ed.EventName,
                            EventDescription = ed.EventDescription,
                            EventDate = ed.EventDate,
                            EmployeeID = ed.PocId,
                            EmployeeName = ad.AssociateName,
                            StartTime = ed.StartTime,
                            EndTime = ed.EndTime,
                            Address = ed.Address,
                            TransportTypeId = ed.TransportTypeId,
                            //TransportType=t.TransportType,
                            BoardingPoint = ed.BoardingPoint,
                            DropPoint = ed.DropPoint,
                            ProjectName = ed.ProjectName,
                            EventCategory = ed.EventCategory,
                            PocId = ed.PocId,
                            VolunteersRequired = ed.VolunteersRequired,
                            FavEvent = ed.FavouriteEvent,
                            EventStatus = ed.EventStatus
                        });
            return data;
        }

        public IQueryable<ViewEventDetails> GetEventInformation()
        {
            var data = (from ed in db.EventDetails
                        join er in db.EventRegistrations on ed.EventId equals er.EventId
                        join vd in db.VolunteerDetails on ed.EventId equals vd.EventId
                        select new ViewEventDetails
                        {
                            EventId = ed.EventId,
                            EventCode = ed.EventCode,
                            LocationName = ed.LocationName,
                            BeneficiaryName = ed.BeneficiaryName,
                            CouncilName = ed.CouncilName,
                            EventName = ed.EventName,
                            EventDescription = ed.EventDescription,
                            EventDate = ed.EventDate,
                            EmployeeID = er.EmployeeID,
                            EmployeeName = er.EmployeeName,
                            StartTime = ed.StartTime,
                            EndTime = ed.EndTime,
                            Address = ed.Address,
                           // TransportTypeId = ed.TransportTypeId,
                           // BoardingPoint = ed.BoardingPoint,
                           // DropPoint = ed.DropPoint,
                            ProjectName = ed.ProjectName,
                            EventCategory = ed.EventCategory,
                            PocId = ed.PocId,
                            VolunteerHours = vd.VolunteerHours,
                            TravelHours = vd.TravelHours,
                            LivesImpacted=vd.LivesImpacted,
                            EventStatus = ed.EventStatus,
                            BusineeUnit=(from bu in db.BusinessUnits where bu.Id ==er.BusinessUnitId select bu.BusinessUnitName ).FirstOrDefault()
                        });
            return data;

        }
        public IQueryable<ChartData> GetChartDataForVolunteerReq()
        {
            var list = (from ev in db.EventDetails
                       where ev.EventStatus.ToUpper()!="APPROVED"
                       group ev by new { ev.EventId,ev.EventDate} into grp
                       select new
                       {
                           EventCount=grp.Sum(x=>x.VolunteersRequired),
                           EventDate=grp.Key.EventDate

                       }).ToList()
 
        .Select(u => new ChartData
        {
            EventCount = u.EventCount,

            EventDate = u.EventDate
        }).ToList().AsQueryable();

            return list;
        }
        public IQueryable<ViewEventDetails> GetEventSummary()
        {
            var data = (from vd in db.VolunteerDetails
                        group vd by new { vd.EventId, vd.Event.EventDate.Month } into detail

                        select new
                        {
                            EventId = detail.Key.EventId,
                            EventMonth= detail.Key.Month.ToString(),
                            EventCode = (from ec in db.EventDetails where ec.EventId == detail.Key.EventId select ec.EventCode).FirstOrDefault(),
                            LocationName = (from ec in db.EventDetails where ec.EventId == detail.Key.EventId select ec.LocationName).FirstOrDefault(),
                            BeneficiaryName = (from ec in db.EventDetails where ec.EventId == detail.Key.EventId select ec.BeneficiaryName).FirstOrDefault(),
                            CouncilName = (from ec in db.EventDetails where ec.EventId == detail.Key.EventId select ec.CouncilName).FirstOrDefault(),
                            EventName = (from ec in db.EventDetails where ec.EventId == detail.Key.EventId select ec.EventName).FirstOrDefault(),
                            EventDescription = (from ec in db.EventDetails where ec.EventId == detail.Key.EventId select ec.EventDescription).FirstOrDefault(),
                            EventDate = (from ec in db.EventDetails where ec.EventId == detail.Key.EventId select ec.EventDate).FirstOrDefault(),
                            Address = (from ec in db.EventDetails where ec.EventId == detail.Key.EventId select ec.Address).FirstOrDefault(),
                            ProjectName = (from ec in db.EventDetails where ec.EventId == detail.Key.EventId select ec.ProjectName).FirstOrDefault(),
                            EventCategory = (from ec in db.EventDetails where ec.EventId == detail.Key.EventId select ec.EventCategory).FirstOrDefault(),
                            PocIds =  db.EventDetails.Where(p => p.EventId == detail.Key.EventId)
                                 .Select(p => p.PocId.ToString()),
                            PocName = db.EventDetails.Where(p => p.EventId == detail.Key.EventId)
                                 .Select(p => p.PocName.AssociateName.ToString()),

                            PocContactNumber =  db.EventDetails.Where(p => p.EventId == detail.Key.EventId)
                                 .Select(p => p.PocName.ContactNumber.ToString()),
                            EventStatus = (from ec in db.EventDetails where ec.EventId == detail.Key.EventId select ec.EventStatus).FirstOrDefault(),
                            TotalVolunteerCount = (from ec in db.EventDetails where ec.EventId == detail.Key.EventId select ec.VolunteersRequired).Sum(),
                            TotalVolunteerHours = detail.Sum(x => x.VolunteerHours),
                            TotalTravelHours = detail.Sum(x => x.TravelHours),
                            OverAllVolunteerHours = detail.Sum(x => x.VolunteerHours) + detail.Sum(x => x.TravelHours),
                            LivesImpacted = detail.Sum(x => x.LivesImpacted)


                        }).ToList().Select(x => new ViewEventDetails()
                        {
                            EventId = x.EventId,
                            EventMonth = x.EventMonth,
                            EventCode = x.EventCode,
                            LocationName = x.LocationName,
                            BeneficiaryName =x.BeneficiaryName,
                            CouncilName =x.CouncilName,
                            EventName =x.EventName,
                            EventDescription = x.EventDescription,
                            EventDate =x.EventDate,
                            Address = x.Address,
                            ProjectName =x.ProjectName,
                            EventCategory = x.EventCategory,
                            PocIds = string.Join(",",x.PocIds),
                            PocName = string.Join(",",x.PocName),
                            PocContactNumber =string.Join(",",x.PocContactNumber),
                            EventStatus = x.EventStatus,
                            TotalVolunteerCount = x.TotalVolunteerCount,
                            TotalVolunteerHours = x.TotalVolunteerHours,
                            TotalTravelHours = x.TotalTravelHours,
                            OverAllVolunteerHours = x.OverAllVolunteerHours,
                            LivesImpacted = x.LivesImpacted
                        }).AsQueryable();
            
            return data;

        }

        public IQueryable<ViewEventDetails> GetEventListByDateAndPocid(String emailId)
        {
            var pocId = (from asso in db.AssociateDetails
                         where asso.EmailId == emailId
                         select asso.EmployeeID);


            var data = (from ed in db.EventDetails
                        where ed.PocId.Equals(pocId.FirstOrDefault())
                        select new ViewEventDetails
                        {
                            EventId = ed.EventId,
                            EventCode = ed.EventCode,
                            LocationName = ed.LocationName,
                            BeneficiaryName = ed.BeneficiaryName,
                            CouncilName = ed.CouncilName,
                            EventName = ed.EventName,
                            EventDescription = ed.EventDescription,
                            EventDate = ed.EventDate,
                            StartTime = ed.StartTime,
                            EndTime = ed.EndTime,
                            Address = ed.Address,
                            TransportTypeId = ed.TransportTypeId,
                            BoardingPoint = ed.BoardingPoint,
                            DropPoint = ed.DropPoint,
                            ProjectName = ed.ProjectName,
                            EventCategory = ed.EventCategory,
                            PocId = ed.PocId,
                            VolunteersRequired = ed.VolunteersRequired,
                            FavEvent = ed.FavouriteEvent,
                            EventStatus = ed.EventStatus,
                            EmployeeID = ed.PocId,


                        });

            return data;


        }

        public IQueryable<EventDetails> GetEventListByDateAndPocid(String emailId, DateTime eventDate)
        {
            var pocId = (from asso in db.AssociateDetails
                         where asso.EmailId == emailId
                         select asso.EmployeeID);

            var data = db.EventDetails.Where(ed => ed.EventDate == eventDate && ed.PocId.Equals(pocId));

            return data;

        }

        public IQueryable<EventDetails> GetTopLocationList(DateTime eventDate)
        {
            var data = (from ed in db.EventDetails
                        where ed.EventDate == eventDate
                        group ed by ed.LocationName into loc
                        orderby loc.Count() descending
                        select new
                        {
                            LocationName = loc.Key,
                            TopLocCount = loc.Count()

                        }).ToList().Select(x => new EventDetails()
                        {
                            LocationName = x.LocationName,
                            TopLocCount = x.TopLocCount
                        }
                        ).ToList().AsQueryable();
            return data;
        }
        public IQueryable<ViewEventRegistrations> GetTopTeamList(DateTime eventDate)
        {
            var data = (from er in db.EventRegistrations
                        join ed in db.EventDetails on er.EventId equals ed.EventId
                        where ed.EventDate == eventDate
                        group er by er.BusinessUnitId into bu
                        select new
                        {
                            BusineeUnit = (from b in db.BusinessUnits where b.Id == bu.Key select b.BusinessUnitName).FirstOrDefault(),
                            TopTeamCount = bu.Count()

                        }).ToList().Select(x => new ViewEventRegistrations()
                        {
                            BusineeUnit = x.BusineeUnit,
                            TopTeamCount = x.TopTeamCount
                        }
                        ).ToList().AsQueryable();
            return data;
        }
        public IQueryable<ViewEventDetails> GetFavEventDetails(bool favEvent)
        {
            var data = (from ed in db.EventDetails
                        join
t in db.Transports on ed.TransportTypeId equals t.Id
                        join
ad in db.AssociateDetails on ed.PocId equals ad.EmployeeID
                        where ed.FavouriteEvent == favEvent
                        //&& ad.EmployeeID == associateID
                        select new ViewEventDetails
                        {
                            EventId = ed.EventId,
                            EventCode = ed.EventCode,
                            LocationName = ed.LocationName,
                            BeneficiaryName = ed.BeneficiaryName,
                            CouncilName = ed.CouncilName,
                            EventName = ed.EventName,
                            EventDescription = ed.EventDescription,
                            EventDate = ed.EventDate,
                            EmployeeID = ed.PocId,
                            EmployeeName = ad.AssociateName,
                            StartTime = ed.StartTime,
                            EndTime = ed.EndTime,
                            Address = ed.Address,
                            TransportTypeId = ed.TransportTypeId,
                            //TransportType = t.TransportType,
                            BoardingPoint = ed.BoardingPoint,
                            DropPoint = ed.DropPoint,
                            ProjectName = ed.ProjectName,
                            EventCategory = ed.EventCategory,
                            PocId = ed.PocId,
                            VolunteersRequired = ed.VolunteersRequired

                        });
            return data;
        }

        public IQueryable<ViewEventRegistrations> GetUnRegisterEventDetails(int associateID, int participationId)
        {
            var data = (from er in db.EventRegistrations
                        join
                        ed in db.EventDetails on er.EventId equals ed.EventId
                        join
ad in db.AssociateDetails on er.EmployeeID equals ad.EmployeeID
                        where er.ParticipationId == 2
                        select new ViewEventRegistrations
                        {
                            Id = er.Id,
                            EventCode = ed.EventCode,
                            EventName = ed.EventName,
                            EventDescription = ed.EventDescription,
                            EventDate = ed.EventDate,
                            StartTime = ed.StartTime,
                            LocationName = ed.LocationName,
                            EndTime = ed.EndTime,
                            AssociateDetailsId = er.AssociateDetailsId,
                            BusinessUnitId = er.BusinessUnitId,
                            ParticipationId = er.ParticipationId,
                            WaitingList = er.WaitingList,
                            EventId = ed.EventId,
                            EmployeeID = ad.EmployeeID,
                            EmployeeName = ad.AssociateName,
                            ContactNumber = ad.ContactNumber,
                            Email = ad.EmailId,
                            BeneficiaryName = ed.BeneficiaryName
                        });
            return data;
        }

        public IQueryable<ViewEventRegistrations> GetNotAttendedEventDetails()
        {
            var data = (from er in db.EventRegistrations
                        join
                        ed in db.EventDetails on er.EventId equals ed.EventId
                        join
ad in db.AssociateDetails on er.EmployeeID equals ad.EmployeeID
                        where er.ParticipationId == 4
                        select new ViewEventRegistrations
                        {
                            Id = er.Id,
                            EventCode = ed.EventCode,
                            EventName = ed.EventName,
                            EventDescription = ed.EventDescription,
                            EventDate = ed.EventDate,
                            StartTime = ed.StartTime,
                            LocationName = ed.LocationName,
                            EndTime = ed.EndTime,
                            AssociateDetailsId = er.AssociateDetailsId,
                            BusinessUnitId = er.BusinessUnitId,
                            ParticipationId = er.ParticipationId,
                            WaitingList = er.WaitingList,
                            EventId = ed.EventId,
                            EmployeeID = ad.EmployeeID,
                            EmployeeName = ad.AssociateName,
                            ContactNumber = ad.ContactNumber,
                            Email = ad.EmailId,
                            BeneficiaryName = ed.BeneficiaryName
                        });
            return data;
        }

        public void CreateEventRegistrations(EventRegistrations eventRegistrations)
        {
            if (eventRegistrations == null)
            {
                throw new NotImplementedException("Not initialized");
            }
            else
            {
                eventRegistrations.ParticipationId = 1;
                eventRegistrations.WaitingList = false;

                if (eventRegistrations.AssociateDetailsId >= 0)
                {
                    var id = from s in db.AssociateDetails
                             join er in db.EventRegistrations
on s.EmployeeID equals er.Associate.EmployeeID
                             select s.Id;
                    if (id.ToList().Count > 0)
                        eventRegistrations.AssociateDetailsId = id.ToList()[0];
                    else
                    {
                        AssociateDetails associate = new AssociateDetails()
                        {
                            AssociateName = eventRegistrations.EmployeeName,
                            EmployeeID = eventRegistrations.EmployeeID,
                            ContactNumber = eventRegistrations.ContactNumber,
                            EmailId = eventRegistrations.EmailId
                            // RoleId = 31
                        };
                        db.AssociateDetails.Add(associate);
                        db.SaveChanges();

                        var newId = from s in db.AssociateDetails
                                    where s.EmployeeID == eventRegistrations.EmployeeID
                                    select s.Id;
                        if (newId.ToList().Count > 0)
                            eventRegistrations.AssociateDetailsId = newId.ToList()[0];
                    }
                }

                db.EventRegistrations.Add(eventRegistrations);
                db.SaveChanges();
                eventRegistrations.Event = GetEventById(eventRegistrations.EventId).FirstOrDefault();

                string emailBody = GetEventRegistrationEmailBody(eventRegistrations);
                Email.SendEmail(eventRegistrations.EmailId, string.Empty, "Outreach Notification for you: Thank you for registering to volunteer", emailBody);
            }

        }
        public void BulkUplaodRegistrationDetails(List<EventRegistrations> eventRegistrations)
        {
            if (eventRegistrations == null)
            {
                throw new NotImplementedException("Not initialized");
            }
            else
            {
                foreach (var eventRegistration in eventRegistrations)
                {

                    if (eventRegistration.AssociateDetailsId >= 0)
                    {
                        var id = from s in db.AssociateDetails
                                 join er in db.EventRegistrations
    on s.EmployeeID equals er.Associate.EmployeeID
                                 select s.Id;
                        if (id.ToList().Count > 0)
                            eventRegistration.AssociateDetailsId = id.ToList()[0];
                        else
                        {
                            AssociateDetails associate = new AssociateDetails()
                            {
                                AssociateName = eventRegistration.EmployeeName,
                                EmployeeID = eventRegistration.EmployeeID,
                                ContactNumber = eventRegistration.ContactNumber,
                                EmailId = eventRegistration.EmailId
                                // RoleId = 31
                            };
                            db.AssociateDetails.Add(associate);
                            db.SaveChanges();

                            var newId = from s in db.AssociateDetails
                                        where s.EmployeeID == eventRegistration.EmployeeID
                                        select s.Id;
                            if (newId.ToList().Count > 0)
                                eventRegistration.AssociateDetailsId = newId.ToList()[0];
                        }
                    }

                    db.EventRegistrations.Add(eventRegistration);
                    db.SaveChanges();
                    eventRegistration.Event = GetEventById(eventRegistration.EventId).FirstOrDefault();
                    string emailBody = GetEventRegistrationEmailBody(eventRegistration);
                    Email.SendEmail(eventRegistration.EmailId, string.Empty, "Outreach Notification for you: Thank you for registering to volunteer", emailBody);

                }
            }

        }


        public IQueryable<EventDetails> GetEventById(int eventId)
        {
            //Get Event Details
            var data = (from ed in db.EventDetails.Include(a => a.TransportType)
                        where ed.EventId == eventId
                        select ed);

            if (data != null)
            {
                //Get POC Details 
                data.FirstOrDefault().PocName = (from ed in db.EventDetails
                                                 join ad in db.AssociateDetails
                                                 on ed.PocId equals ad.EmployeeID
                                                 where ed.EventId == eventId
                                                 select ad).FirstOrDefault();
            }
            return data;
        }


        public void CreateFutureAvailability(FutureAvailability futureAvailability)
        {
            if (futureAvailability == null)
            {
                throw new NotImplementedException("Not initialized");
            }
            else
            {
                db.FutureAvailability.Add(futureAvailability);
                db.SaveChanges();
            }
        }

        public void NotifyUpcomingEvents (List<BusinessUnit> toList)
        {
            DateTime eventEndDate = DateTime.Now.AddDays(30);
            IQueryable<EventDetails> eventDetails = db.EventDetails.AsNoTracking().Where(r => r.EventDate > eventEndDate);
            if (!eventDetails.Any())
                return;
            string emailBody = GetUpcomingEventsEmailBody(eventDetails);
            foreach (var mail in toList)
            {
                mail.BusinessUnitName = "krithi.ravi20@gmail.com";
                SendMail(mail.BusinessUnitName, "krithi.ravi20@gmail.com", string.Empty, "Upcoming Outreach events for you", emailBody);
            }

        }

        public string SendMail(string toList, string from, string ccList, string subject, string body)
        {
            MailMessage message = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            string msg = string.Empty;
            try
            {
                MailAddress fromAddress = new MailAddress(from);
                message.From = fromAddress;
                message.To.Add(toList);
                if (ccList != null && ccList != string.Empty)
                    message.CC.Add(ccList);
                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = body;
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.Port = 587;
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.Credentials = new System.Net.NetworkCredential(fromAddress.Address, "dot@KMU12");

                smtpClient.Send(message);
                msg = "Successful<BR>";
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return msg;
        }


        private string GetUpcomingEventsEmailBody(IQueryable<EventDetails> lstEventDetails)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Dear <b>Associate</b><br/><br/>");
            sb.Append("Here are some upcoming events based on your preferences that require volunters:<br/><br/>");
            sb.Append("Please find below some upcoming Outreach events we thought you'd like to participate in <br/><br/>");


            sb.Append("<table style='border:1px solid black;border-collapse:collapse'><tr style='border:1px solid black'>" +
                "<td style='border:1px solid black'><b>Event Name</b></td>" +
                "<td style='border:1px solid black'><b>Event Schedule</b></td>" +
                "<td style='border:1px solid black'><b>Venue</b></td>" +
                "<td style='border:1px solid black'><b>Registration Link</b></td></tr>");

            lstEventDetails.ToList().ForEach(eventDetails =>
            {
                sb.Append(string.Format("<tr style='border:1px solid black'>" +
                    "<td style='border:1px solid black'>{0}</td>" +
                    "<td style='border:1px solid black'>{1}</td>" +
                    "<td style='border:1px solid black'>{2}</td>" +
                    "<td style='border:1px solid black'>{3}</td></tr>",
                    eventDetails.EventName, "Event Schedule", eventDetails.LocationName,
                    string.Format("<a href='{0}/EventRegistration'>Click Here</a>", Convert.ToString(ConfigurationManager.AppSettings["ApplicationUrl"]))));

                //TODO: Get the application url and wire up the appropriate page in href.
            });

            sb.Append("</table><br/><br/>");
            sb.Append("Visit the volunteer profile in the Outreach app to know your volunteering details and set notification preferences. " +
                "Volunteer efforts will appear in the profile page only after it is submitted by Event Poc. <br/><br/>");
            sb.Append("<b>Best Wishes, <br/> Outreach Team </b>");
            return sb.ToString();
        }

        private string GetEventRegistrationEmailBody(EventRegistrations eventRegistrations)
        {
            string pocName = string.Empty, contact = string.Empty;
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("Hi <b>{0}</b><br/><br/>", eventRegistrations.EmployeeName));

            sb.Append(string.Format("Thank you for your interest to volunteer for {0}. Please find below the details of the event.<br/><br/>", eventRegistrations.Event.EventName));

            sb.Append(string.Format("<b>Date:</b> {0}<br/><br/>", eventRegistrations.Event.EventDate.ToShortDateString()));

            DateTime time = DateTime.Today.Add(eventRegistrations.Event.StartTime);
            sb.Append(string.Format("<b>Time:</b> {0}<br/><br/>", time.ToString("hh:mm tt")));

            sb.Append(string.Format("<b>Event Description:</b> {0}<br/><br/>", eventRegistrations.Event.EventDescription));

            sb.Append(string.Format("<b>Transport Provided:</b> {0}<br/><br/>", eventRegistrations.Event.TransportType.TransportType));

            sb.Append(string.Format("<b>Venue:</b> {0}<br/><br/>", eventRegistrations.Event.LocationName));
            if (eventRegistrations != null && eventRegistrations.Event != null && eventRegistrations.Event.PocName != null)
            {
                pocName = eventRegistrations.Event.PocName.AssociateName;
                contact = Convert.ToString(eventRegistrations.Event.PocName.ContactNumber);
            }
            sb.Append(string.Format("For more details on this event please contact: {0} ({1})<br/><br/>", pocName, contact));

            //TODO: Application host url to be added in href
            sb.Append(string.Format("If you are unable to make it to the event, please click here to <a href='" + Convert.ToString(ConfigurationManager.AppSettings["ApplicationUrl"]) + "/ViewRegistration/{0}'>Unregister</a><br/><br/>", eventRegistrations.Id));

            sb.Append("<b>Thank You, <br/>Outreach</b>");
            return sb.ToString();
        }

        private string GetUnregisterEmailBody(EventRegistrations eventRegistrations)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("Hi <b>{0}</b><br/><br/>", eventRegistrations.EmployeeName));

            sb.Append(string.Format("Your un-registration is confirmed for the event {0}.<br/><br/>", eventRegistrations.Event.EventName));
            sb.Append("You can still show us your support by doing a kindness act!<br/><br/>Please find below the details of the event:<br/><br/>");
            sb.Append(string.Format("<b>Date:</b> {0}<br/><br/>", eventRegistrations.Event.EventDate.ToShortDateString()));

            DateTime time = DateTime.Today.Add(eventRegistrations.Event.StartTime);
            sb.Append(string.Format("<b>Time:</b> {0}<br/><br/>", time.ToString("hh:mm tt")));

            sb.Append(string.Format("<b>Event Description:</b> {0}<br/><br/>", eventRegistrations.Event.EventDescription));

            sb.Append(string.Format("<b>Transport Provided:</b> {0}<br/><br/>", eventRegistrations.Event.TransportType.TransportType));

            sb.Append(string.Format("<b>Venue:</b> {0}<br/><br/>", eventRegistrations.Event.LocationName));

            sb.Append("If you wish to look for other events, navigate to <b>Upcoming Events</b> on the calendar<br/><br/>");
            sb.Append("<b>Thank You, <br/>Outreach</b>");
            return sb.ToString();
        }

        private string GetEventRejectedEmailBody(EventDetails eventDetails)
        {
            string pocName = string.Empty, contact = string.Empty;
            StringBuilder sb = new StringBuilder();

            if (eventDetails != null && eventDetails.PocName != null)
                pocName = eventDetails.PocName.AssociateName;

            sb.Append(string.Format("Hi <b>{0}</b><br/><br/>", pocName));

            sb.Append("We regret to inform that the event you created has been rejected by Outreach Admin. <br/><br/>");
            sb.Append("Please find below the details of the event.<br/><br/>");

            sb.Append(string.Format("<b>Event Name:</b> {0}<br/><br/>", eventDetails.EventName));
            sb.Append(string.Format("<b>Event Description:</b> {0}<br/><br/>", eventDetails.EventDescription));


            sb.Append(string.Format("<b>Date:</b> {0}<br/><br/>", eventDetails.EventDate.ToShortDateString()));

            DateTime time = DateTime.Today.Add(eventDetails.StartTime);
            sb.Append(string.Format("<b>Time:</b> {0}<br/><br/>", time.ToString("hh:mm tt")));

            if (eventDetails != null && eventDetails.TransportType != null)
            {
                sb.Append(string.Format("<b>Transport Provided:</b> {0}<br/><br/>", eventDetails.TransportType.TransportType));
            }

            sb.Append(string.Format("<b>Venue:</b> {0}<br/><br/>", eventDetails.LocationName));
            
            sb.Append("<b>Thank You, <br/>Outreach</b>");
            return sb.ToString();
        }

        public void BulkUploadVolunteerDetails(List<VolunteerDetails> volunteerDetails)
        {
            if (volunteerDetails == null)
            {
                throw new NotImplementedException("Not initialized");
            }
            else
            {
                foreach (var volunteerDetail in volunteerDetails)
                {
                    //volunteerDetail.EmployeeID = volunteerDetail.AssociateDetailsId;
                    //volunteerDetail.AssociateDetailsId = db.AssociateDetails.ToList().Where(c => c.EmployeeID == volunteerDetail.AssociateDetailsId).FirstOrDefault().Id;
                    //db.VolunteerDetails.Add(volunteerDetail);
                    //Check if the associate details already exists; If not save;
                    AssociateDetails associateDetails = db.AssociateDetails.FirstOrDefault(c => c.EmployeeID == volunteerDetail.EmployeeID);
                    if (associateDetails == null)
                    {
                        associateDetails = new AssociateDetails();
                        associateDetails.EmployeeID = volunteerDetail.EmployeeID;
                        associateDetails.AssociateName = volunteerDetail.EmployeeName;
                        associateDetails.ContactNumber = volunteerDetail.ContactNumber;
                        associateDetails.EmailId = volunteerDetail.EmailId;
                        db.AssociateDetails.Add(associateDetails);
                        db.SaveChanges();
                    }
                    //Update the pariticipation details in event registration
                    EventRegistrations eventRegistrations = db.EventRegistrations.FirstOrDefault(r => r.AssociateDetailsId == associateDetails.Id &&
                                                        r.EventId == volunteerDetail.EventId);

                    if (eventRegistrations != null &&
                        eventRegistrations.ParticipationId != volunteerDetail.ParticipationId)
                    {
                        eventRegistrations.ParticipationId = volunteerDetail.ParticipationId;
                        db.Entry(eventRegistrations).State = EntityState.Modified;
                    }

                    //Check if associate is saved for the same volunteer event;
                    VolunteerDetails objVolunteerDetails = db.VolunteerDetails.FirstOrDefault(r => r.Associate.Id == associateDetails.Id &&
                                                           r.EventId == volunteerDetail.EventId);
                    if (objVolunteerDetails == null)
                    {
                        volunteerDetail.Associate = associateDetails;
                        db.VolunteerDetails.Add(volunteerDetail);
                    }
                    else
                    {
                        objVolunteerDetails.LivesImpacted = volunteerDetail.LivesImpacted;
                        objVolunteerDetails.ParticipationId = volunteerDetail.ParticipationId;
                        objVolunteerDetails.TravelHours = volunteerDetail.TravelHours;
                        objVolunteerDetails.VolunteerHours = volunteerDetail.VolunteerHours;
                        db.Entry(objVolunteerDetails).State = EntityState.Modified;
                    }


                }

                db.SaveChanges();
               
            }
        }

        public void CreateVolunteerDetails(VolunteerDetails volunteerDetails)
        {
            if (volunteerDetails == null)
            {
                throw new NotImplementedException("Not initialized");
            }
            else
            {
                //Check if the associate details already exists; If not save;
                AssociateDetails associateDetails = db.AssociateDetails.FirstOrDefault(c => c.EmployeeID == volunteerDetails.EmployeeID);
                if (associateDetails == null)
                {
                    associateDetails = new AssociateDetails();
                    associateDetails.EmployeeID = volunteerDetails.EmployeeID;
                    associateDetails.AssociateName = volunteerDetails.EmployeeName;
                    associateDetails.ContactNumber = volunteerDetails.ContactNumber;
                    associateDetails.EmailId = volunteerDetails.EmailId;
                    db.AssociateDetails.Add(associateDetails);
                    db.SaveChanges();
                }
                //Update the pariticipation details in event registration
                EventRegistrations eventRegistrations = db.EventRegistrations.FirstOrDefault(r => r.AssociateDetailsId == associateDetails.Id &&
                                                    r.EventId == volunteerDetails.EventId);

                if (eventRegistrations != null &&
                    eventRegistrations.ParticipationId != volunteerDetails.ParticipationId)
                {
                    eventRegistrations.ParticipationId = volunteerDetails.ParticipationId;
                    db.Entry(eventRegistrations).State = EntityState.Modified;
                }

                //Check if associate is saved for the same volunteer event;
                VolunteerDetails objVolunteerDetails = db.VolunteerDetails.FirstOrDefault(r => r.Associate.Id == associateDetails.Id &&
                                                       r.EventId == volunteerDetails.EventId);
                if (objVolunteerDetails == null)
                {
                    volunteerDetails.Associate = associateDetails;
                    db.VolunteerDetails.Add(volunteerDetails);
                }
                else
                {
                    objVolunteerDetails.LivesImpacted = volunteerDetails.LivesImpacted;
                    objVolunteerDetails.ParticipationId = volunteerDetails.ParticipationId;
                    objVolunteerDetails.TravelHours = volunteerDetails.TravelHours;
                    objVolunteerDetails.VolunteerHours = volunteerDetails.VolunteerHours;
                    db.Entry(objVolunteerDetails).State = EntityState.Modified;
                }
                db.SaveChanges();

            }
        }
        public IQueryable<Roles> GetRoles()
        {
            var roles = (from role in db.Roles
                         select new
                         {
                             RoleId = role.RoleId,
                             RoleType = role.RoleType
                         }).ToList().Select(x => new Roles()
                         {
                             RoleId = x.RoleId,
                             RoleType = x.RoleType
                         }).AsQueryable();
            return roles;
        }
        public IQueryable<BusinessUnit> GetBuList()
        {
            var roles = (from bu in db.BusinessUnits
                         select new
                         {
                             Id = bu.Id,
                             BusinessUnitName = bu.BusinessUnitName
                         }).ToList().Select(x => new BusinessUnit()
                         {
                             Id = x.Id,
                             BusinessUnitName = x.BusinessUnitName
                         }).AsQueryable();
            return roles;
        }

        private string GetEventCreationEmaiBody(EventDetails eventDetails)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("Hi <b>{0}</b><br/><br/>", eventDetails.PocName));

            sb.Append(string.Format("Event <b>{0}</b> has been created successfully. Please find below the details of the event.<br/><br/>", eventDetails.EventName));

            sb.Append(string.Format("<b>Date:</b> {0}<br/><br/>", eventDetails.EventDate.ToShortDateString()));

            DateTime time = DateTime.Today.Add(eventDetails.StartTime);
            sb.Append(string.Format("<b>Time:</b> {0}<br/><br/>", time.ToString("hh:mm tt")));

            sb.Append(string.Format("<b>Event Description:</b> {0}<br/><br/>", eventDetails.EventDescription));

            sb.Append(string.Format("<b>Beneficiary Name:</b> {0}<br/><br/>", eventDetails.BeneficiaryName));

            sb.Append(string.Format("<b>Volunteers Required:</b> {0}<br/><br/>", eventDetails.VolunteersRequired));

            sb.Append(string.Format("<b>Transport Provided:</b> {0}<br/><br/>", eventDetails.TransportType));

            sb.Append(string.Format("<b>Venue:</b> {0}<br/><br/>", eventDetails.LocationName));

            sb.Append("<b>Thank You, <br/>Outreach</b>");
            return sb.ToString();

        }

      
        public IQueryable<ChartData> GetChartData()
        {
            var list = db.EventDetails.Where(x => x.EventStatus == "APPROVED").GroupBy(u => u.EventDate.Month) 
       .Select(u => new ChartData
       {
           EventCount = u.Count(),
           Month = u.FirstOrDefault().EventDate.Month.ToString()
       }).ToList().AsQueryable();

            foreach (var userdata in list)
            {
                switch (userdata.Month)
                {
                    case "1":
                        userdata.Month = "Jan";
                        break;
                    case "2":
                        userdata.Month = "Feb";
                        break;
                    case "3":
                        userdata.Month = "Mar";
                        break;
                    case "4":
                        userdata.Month = "Apr";
                        break;
                    case "5":
                        userdata.Month = "May";
                        break;
                    case "6":
                        userdata.Month = "Jun";
                        break;
                    case "7":
                        userdata.Month = "Jul";
                        break;
                    case "8":
                        userdata.Month = "Aug";
                        break;
                    case "9":
                        userdata.Month = "Sep";
                        break;
                    case "10":
                        userdata.Month = "Oct";
                        break;
                    case "11":
                        userdata.Month = "Nov";
                        break;
                    case "12":
                        userdata.Month = "Dec";
                        break;
                    default:
                        userdata.Month = "error";
                        break;

                }

            }
            return list;
        }

        
        public IQueryable<ChartData> GetPieChartData()
        {
            var list = (from ev in db.EventRegistrations
                       join ed in db.EventDetails on ev.EventId equals ed.EventId 
                       group ev by ev.BusinessUnitId into bu
                        select new
                        {
                            BusineeUnit = (from b in db.BusinessUnits where b.Id == bu.Key select b.BusinessUnitName).FirstOrDefault(),
                            TopTeamCount = bu.Count()

                        }).ToList().Select(x => new ChartData()
                        {
                            ProjectTeam = x.BusineeUnit,
                            RegistrationCount = x.TopTeamCount
                        }
                        ).ToList().AsQueryable();
            return list;
        }
        public IQueryable<ChartData> GetPocChartData(string emailId)
        {
            var pocId = (from asso in db.AssociateDetails
                         where asso.EmailId == emailId
                         select asso.EmployeeID);

            var list = (from ev in db.EventRegistrations
                        join ed in db.EventDetails on ev.EventId equals ed.EventId
                        where ed.PocId == pocId.FirstOrDefault()
                        group ev by ev.BusinessUnitId into bu
                        select new
                        {
                            BusineeUnit = (from b in db.BusinessUnits where b.Id == bu.Key select b.BusinessUnitName).FirstOrDefault(),
                            TopTeamCount = bu.Count()

                        }).ToList().Select(x => new ChartData()
                        {
                            ProjectTeam = x.BusineeUnit,
                            RegistrationCount = x.TopTeamCount
                        }
                        ).ToList().AsQueryable();
            return list;
        }
    }
}



