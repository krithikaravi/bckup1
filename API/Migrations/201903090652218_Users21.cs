namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Users21 : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.Users", "UserRoles_Id", "dbo.UserRoles");
            //DropIndex("dbo.Users", new[] { "UserRoles_Id" });
            //DropTable("dbo.Users");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeID = c.Int(nullable: false),
                        UserName = c.String(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        ContactNumber = c.Long(nullable: false),
                        EmailId = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        UserRoles_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Users", "UserRoles_Id");
            AddForeignKey("dbo.Users", "UserRoles_Id", "dbo.UserRoles", "Id");
        }
    }
}
