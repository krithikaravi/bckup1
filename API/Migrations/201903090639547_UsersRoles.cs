namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsersRoles : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleId = c.Int(nullable: false, identity: true),
                        RoleType = c.String(nullable: false),
                        UserRoles_Id = c.Int(),
                    })
                .PrimaryKey(t => t.RoleId)
                .ForeignKey("dbo.UserRoles", t => t.UserRoles_Id)
                .Index(t => t.UserRoles_Id);
            
            AddColumn("dbo.UserRoles", "EmployeeID", c => c.Int(nullable: false));
            AddColumn("dbo.UserRoles", "RoleId", c => c.Int(nullable: false));
            DropColumn("dbo.UserRoles", "RoleType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserRoles", "RoleType", c => c.String(nullable: false));
            DropForeignKey("dbo.Roles", "UserRoles_Id", "dbo.UserRoles");
            DropIndex("dbo.Roles", new[] { "UserRoles_Id" });
            DropColumn("dbo.UserRoles", "RoleId");
            DropColumn("dbo.UserRoles", "EmployeeID");
            DropTable("dbo.Roles");
        }
    }
}
