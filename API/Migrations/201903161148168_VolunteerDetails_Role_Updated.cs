namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VolunteerDetails_Role_Updated : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.VolunteerDetails", "AssociateDetailsId", "dbo.AssociateDetails");
            DropIndex("dbo.VolunteerDetails", new[] { "AssociateDetailsId" });
            RenameColumn(table: "dbo.VolunteerDetails", name: "AssociateDetailsId", newName: "Associate_Id");
            AddColumn("dbo.UserRoles", "RoleType", c => c.String());
            AlterColumn("dbo.VolunteerDetails", "Associate_Id", c => c.Int());
            CreateIndex("dbo.VolunteerDetails", "Associate_Id");
            AddForeignKey("dbo.VolunteerDetails", "Associate_Id", "dbo.AssociateDetails", "Id");
            DropColumn("dbo.VolunteerDetails", "EmployeeName");
            DropColumn("dbo.VolunteerDetails", "EmployeeID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.VolunteerDetails", "EmployeeID", c => c.Int(nullable: false));
            AddColumn("dbo.VolunteerDetails", "EmployeeName", c => c.String());
            DropForeignKey("dbo.VolunteerDetails", "Associate_Id", "dbo.AssociateDetails");
            DropIndex("dbo.VolunteerDetails", new[] { "Associate_Id" });
            AlterColumn("dbo.VolunteerDetails", "Associate_Id", c => c.Int(nullable: false));
            DropColumn("dbo.UserRoles", "RoleType");
            RenameColumn(table: "dbo.VolunteerDetails", name: "Associate_Id", newName: "AssociateDetailsId");
            CreateIndex("dbo.VolunteerDetails", "AssociateDetailsId");
            AddForeignKey("dbo.VolunteerDetails", "AssociateDetailsId", "dbo.AssociateDetails", "Id", cascadeDelete: true);
        }
    }
}
