namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FutureAvailabilty2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FutureAvailabilities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Availabilty = c.DateTime(nullable: false),
                        Location = c.String(),
                        EmployeeID = c.Int(nullable: false),
                        AssociateDetails_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssociateDetails", t => t.AssociateDetails_Id)
                .Index(t => t.AssociateDetails_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FutureAvailabilities", "AssociateDetails_Id", "dbo.AssociateDetails");
            DropIndex("dbo.FutureAvailabilities", new[] { "AssociateDetails_Id" });
            DropTable("dbo.FutureAvailabilities");
        }
    }
}
