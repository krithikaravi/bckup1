namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Users3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeID = c.Int(nullable: false),
                        UserName = c.String(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        ContactNumber = c.Long(nullable: false),
                        EmailId = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        UserRoles_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserRoles", t => t.UserRoles_Id)
                .Index(t => t.UserRoles_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "UserRoles_Id", "dbo.UserRoles");
            DropIndex("dbo.Users", new[] { "UserRoles_Id" });
            DropTable("dbo.Users");
        }
    }
}
