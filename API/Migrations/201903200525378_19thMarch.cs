namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _19thMarch : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventRegistrations", "TransportTypeId", c => c.Int(nullable: false));
            AddColumn("dbo.EventRegistrations", "BoardingPoint", c => c.String());
            AddColumn("dbo.EventRegistrations", "DropPoint", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EventRegistrations", "DropPoint");
            DropColumn("dbo.EventRegistrations", "BoardingPoint");
            DropColumn("dbo.EventRegistrations", "TransportTypeId");
        }
    }
}
