namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventStatusIdupdated2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EventDetails", "EventStatusId", "dbo.EventStatus");
            DropIndex("dbo.EventDetails", new[] { "EventStatusId" });
            AddColumn("dbo.EventDetails", "EventStatus", c => c.String());
            DropColumn("dbo.EventDetails", "EventStatusId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EventDetails", "EventStatusId", c => c.Int(nullable: false));
            DropColumn("dbo.EventDetails", "EventStatus");
            CreateIndex("dbo.EventDetails", "EventStatusId");
            AddForeignKey("dbo.EventDetails", "EventStatusId", "dbo.EventStatus", "Id", cascadeDelete: true);
        }
    }
}
