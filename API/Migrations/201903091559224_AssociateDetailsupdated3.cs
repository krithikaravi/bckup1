namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AssociateDetailsupdated3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AssociateDetails", "Password", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AssociateDetails", "Password");
        }
    }
}
