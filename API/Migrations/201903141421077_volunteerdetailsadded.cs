namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class volunteerdetailsadded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VolunteerDetails", "ParticipationId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VolunteerDetails", "ParticipationId");
        }
    }
}
