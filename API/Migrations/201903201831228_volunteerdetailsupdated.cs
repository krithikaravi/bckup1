namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class volunteerdetailsupdated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VolunteerDetails", "EmployeeID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.VolunteerDetails", "EmployeeID");
        }
    }
}
