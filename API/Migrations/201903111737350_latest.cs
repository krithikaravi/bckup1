namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class latest : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EventDetails", "EventName", c => c.String());
            AlterColumn("dbo.EventDetails", "EventDescription", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EventDetails", "EventDescription", c => c.String(nullable: false));
            AlterColumn("dbo.EventDetails", "EventName", c => c.String(nullable: false));
        }
    }
}
