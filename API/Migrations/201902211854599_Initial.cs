namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AssociateDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AssociateName = c.String(nullable: false),
                        ContactNumber = c.String(nullable: false),
                        EmailId = c.String(nullable: false),
                        RoleId = c.Int(nullable: false),
                        Users_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserRoles", t => t.Users_Id)
                .Index(t => t.Users_Id);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleType = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AssociateDetails", "Users_Id", "dbo.UserRoles");
            DropIndex("dbo.AssociateDetails", new[] { "Users_Id" });
            DropTable("dbo.UserRoles");
            DropTable("dbo.AssociateDetails");
        }
    }
}
