namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AssociateDetailsUpdated : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AssociateDetails", "RoleId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AssociateDetails", "RoleId", c => c.Int(nullable: false));
        }
    }
}
