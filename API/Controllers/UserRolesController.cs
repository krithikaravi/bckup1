﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Functions;
using Models.Models;

namespace API.Controllers
{
    public class UserRolesController : ApiController
    {
        private MyDbContext db = new MyDbContext();
        private VolunteerFunctions functions = new VolunteerFunctions();
        // GET: api/UserRoles
        [HttpGet, Route("api/UserRoles")]
        [ResponseType(typeof(UserRoles))]
        public IQueryable<UserRoles> GetUserRoles()
        {
            var role = functions.GetUserRoles();
            return role;
        }

        [HttpGet, Route("api/UserRoles/{id}")]
        [ResponseType(typeof(UserRoles))]
        public IHttpActionResult GetUserRoles(int id)
        {
            UserRoles userRoles = db.UserRoles.Find(id);
            if (userRoles == null)
            {
                return NotFound();
            }

            return Ok(userRoles);
        }

        [HttpPut, Route("api/UserRoles/{id}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUserRoles(int id, UserRoles userRoles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userRoles.EmployeeID)
            {
                return BadRequest();
            }

            db.Entry(userRoles).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserRolesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost, Route("api/UserRoles")]
        [ResponseType(typeof(UserRoles))]
        public IHttpActionResult PostUserRoles(UserRoles userRoles)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                
                if (!UserRolesExists(userRoles.EmployeeID))
                {
                    db.UserRoles.Add(userRoles);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return CreatedAtRoute("DefaultApi", new { id = userRoles.Id }, userRoles);
        }

        [HttpDelete, Route("api/UserRoles/{id}")]
        [ResponseType(typeof(UserRoles))]
        public IHttpActionResult DeleteUserRoles([FromUri]int id)
        {
            try
            {
                UserRoles userRoles = db.UserRoles.Find(id);
                if (userRoles == null)
                {
                    return NotFound();
                }
                else
                {
                    db.UserRoles.Remove(userRoles);
                    db.SaveChanges();

                    return Ok(userRoles);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserRolesExists(int id)
        {
            return db.UserRoles.Any(e => e.EmployeeID == id);
        }
    }
}