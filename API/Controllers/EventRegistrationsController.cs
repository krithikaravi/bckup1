﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
//using System.Web.Http;
using System.Web.Http.Description;
//using System.Web.Mvc;
using API.Functions;
using Models.Models;

namespace API.Controllers
{
    public class EventRegistrationsController : ApiController
    {
        private MyDbContext db = new MyDbContext();
        private VolunteerFunctions functions = new VolunteerFunctions();

        //[Authorize(Roles = "user,admin")]
        [HttpGet, Route("api/EventRegistrations")]
        [ResponseType(typeof(EventDetails))]
        // GET: api/EventRegistrations
        public IQueryable GetEventRegistrations()

        {
            //var identity = (ClaimsIdentity)User.Identity;
            //var roles = identity.Claims
            //    .Where(c => c.Type == ClaimTypes.Role)
            //    .Select(c => c.Value);
            //return db.EventRegistrations;
            return functions.GetRegistrationDetail();
        }

        [HttpGet]
        [Route("api/EventRegistrations/topLocationProjectTeambyDate/{eventDate}")]
        public IQueryable<ViewEventRegistrations> GetTopProjectTeamList(DateTime eventDate)
        {
            //return db.EventDetails.Where(r => r.EventDate == eventDate && r.EventStatus.ToUpper() == "APPROVED");
            var teamDetails = functions.GetTopTeamList(eventDate);
            //var eventNames = eventDetails.Select(c => c.EventName);
            return teamDetails;
        }

        // [Authorize(Roles = "user,admin")]
        [HttpGet, Route("api/EventRegistrations/{id}")]
        // GET: api/EventRegistrations/5
        [ResponseType(typeof(ViewEventRegistrations))]
        public IQueryable GetEventRegistrations(int id)
        {
            //EventRegistrations eventRegistrations = db.EventRegistrations.Find(id);
            //if (eventRegistrations == null)
            //{
            //    return NotFound();
            //}

            //return Ok(eventRegistrations);
            return functions.GetRegistrationDetails(id);
        }

      //  [Authorize(Roles = "user,admin")]
        [HttpGet]
        [ResponseType(typeof(ViewEventRegistrations))]
       // [Route("api/EventRegistrations/associateID/participationId")]
        public IQueryable<ViewEventRegistrations> GetUnRegisterEventDetails(int associateID, int participationId)
        {
            return functions.GetUnRegisterEventDetails(associateID, participationId);
        }

        [HttpGet, Route("api/EventRegistrations/NotAttended")]
        [ResponseType(typeof(ViewEventRegistrations))]
       // [Route("api/EventRegistrations/associateID/participationType/participationId")]
        public IQueryable<ViewEventRegistrations> GetNotAttendedEventDetails()
        {
            return functions.GetNotAttendedEventDetails();
        }

        [HttpPut,Route("api/EventRegistrations/{id}")]
        // PUT: api/EventRegistrations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEventRegistrations(int id, EventRegistrations eventRegistrations)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != eventRegistrations.Id)
            {
                return BadRequest();
            }

            db.Entry(eventRegistrations).State = EntityState.Modified;

            try
            {
                eventRegistrations.ParticipationId = 2;
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventRegistrationsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            GetEventRegistrations(eventRegistrations.EventId);

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpPost,Route ("api/EventRegistrations/SaveEventRegistrations")]
        // POST: api/EventRegistrations
        [ResponseType(typeof(EventRegistrations))]
        public IHttpActionResult PostEventRegistrations(EventRegistrations eventRegistrations)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //db.EventRegistrations.Add(eventRegistrations);
            //db.SaveChanges();

            //return CreatedAtRoute("DefaultApi", new { id = eventRegistrations.Id }, eventRegistrations);

            else
            {
                try
                {
                    
                    functions.CreateEventRegistrations(eventRegistrations);
                    var msg = Request.CreateResponse(HttpStatusCode.Created);
                    msg.Headers.Location = new Uri(Request.RequestUri + eventRegistrations.Id.ToString());
                    return CreatedAtRoute("DefaultApi", new { id = eventRegistrations.Id }, eventRegistrations);
                    //return msg;

                }
                catch (Exception ex)
                {
                    //throw new Exception("error",ex.Message);
                    Console.WriteLine(ex.Message.ToString());
                    throw new Exception("error");
                }
            }
        }
        [HttpPost, Route("api/EventRegistrations/BulkUploadRegistration")]
        [ResponseType(typeof(List<EventRegistrations>))]
        //  [Route("bulkuploaddata")]
        //httppostattribute: api/eventdetails
        public IHttpActionResult PostBulkUploadRegistration([FromBody]List<EventRegistrations> eventRegistrations)
        {
            //if (string.isnullorwhitespace(eventdetails.eventname))
            //{
            //    //return request.createerrorresponse(httpstatuscode.badrequest, "event name can not be empty");
            //    return notfound();
            //}

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                try
                {
                    bool status = false;
                    if (ModelState.IsValid)
                    {
                        functions.BulkUplaodRegistrationDetails(eventRegistrations);
                        status = true;
                        return Ok(status);
                    }
                    else
                    {
                        return BadRequest(ModelState);
                    }

                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message.ToString());
                    throw new Exception("error");
                }
            }
        }
       [HttpDelete,Route("api/EventRegistrations/{id}")]
        [ResponseType(typeof(EventRegistrations))]
        public IHttpActionResult DeleteEventRegistrations(int id)
        {
            EventRegistrations eventRegistrations = db.EventRegistrations.Find(id);
            if (eventRegistrations == null)
            {
                return NotFound();
            }

            db.EventRegistrations.Remove(eventRegistrations);
            db.SaveChanges();
            functions.SendUnregisterNotification(eventRegistrations);
            return Ok(eventRegistrations);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EventRegistrationsExists(int id)
        {
            return db.EventRegistrations.Count(e => e.Id == id) > 0;
        }
    }
}