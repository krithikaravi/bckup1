﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

public class MyDbContext : DbContext
{
    // You can add custom code to this file. Changes will not be overwritten.
    // 
    // If you want Entity Framework to drop and regenerate your database
    // automatically whenever you change your model schema, please use data migrations.
    // For more information refer to the documentation:
    // http://msdn.microsoft.com/en-us/data/jj591621.aspx

    public MyDbContext() : base("name=MyDbContext")
    {
    }

    public System.Data.Entity.DbSet<Models.Models.AssociateDetails> AssociateDetails { get; set; }

    public System.Data.Entity.DbSet<Models.Models.Roles> Roles { get; set; }

    public System.Data.Entity.DbSet<Models.Models.UserRoles> UserRoles { get; set; }

    public System.Data.Entity.DbSet<Models.Models.EventDetails> EventDetails { get; set; }

    public System.Data.Entity.DbSet<Models.Models.Beneficiary> Beneficiaries { get; set; }

    public System.Data.Entity.DbSet<Models.Models.EventCategory> EventCategories { get; set; }

    public System.Data.Entity.DbSet<Models.Models.Council> Councils { get; set; }

    public System.Data.Entity.DbSet<Models.Models.Location> Locations { get; set; }

    public System.Data.Entity.DbSet<Models.Models.Project> Projects { get; set; }

    public System.Data.Entity.DbSet<Models.Models.EventStatus> EventStatus { get; set; }

    public System.Data.Entity.DbSet<Models.Models.Transport> Transports { get; set; }

    public System.Data.Entity.DbSet<Models.Models.EventRegistrations> EventRegistrations { get; set; }

    public System.Data.Entity.DbSet<Models.Models.BusinessUnit> BusinessUnits { get; set; }

    public System.Data.Entity.DbSet<Models.Models.Participation> Participations { get; set; }

    public System.Data.Entity.DbSet<Models.Models.VolunteerDetails> VolunteerDetails { get; set; }
    public System.Data.Entity.DbSet<Models.Models.FutureAvailability> FutureAvailability { get; set; }
    public System.Data.Entity.DbSet<Models.Models.Users> Users { get; set; }

}
