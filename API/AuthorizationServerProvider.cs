﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Cors;

namespace API
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        //public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        //{
        //    context.Validated();
        //}

        //public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        //{
        //    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
        //    if (context.UserName == "admin" && context.Password == "admin")
        //    {
        //        identity.AddClaim(new Claim(ClaimTypes.Role, "admin"));
        //        identity.AddClaim(new Claim("username", "admin"));
        //        identity.AddClaim(new Claim(ClaimTypes.Name, "roshan vankadara"));
        //        context.Validated(identity);

        //    }
        //    else if (context.UserName == "user" && context.Password =="user")
        //    {
        //        identity.AddClaim(new Claim(ClaimTypes.Role, "user"));
        //        identity.AddClaim(new Claim("username", "user"));
        //        identity.AddClaim(new Claim(ClaimTypes.Name, "Ram"));
        //        context.Validated(identity);
        //    }
        //    else
        //    {
        //        context.SetError("invalid grant","provided username and password are incorrect");
        //        return;
        //    }
        //}


        //        publicclassDotNetTechyAuthServerProvider : OAuthAuthorizationServerProvider
        //{
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            // context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            using (var db = new MyDbContext())
            {
                if (db != null)
                {
                    var user = db.AssociateDetails.ToList();
                    if (user != null)
                    {
                        if (!string.IsNullOrEmpty(user.Where(u => u.EmailId == context.UserName && u.Password == context.Password).FirstOrDefault().AssociateName))
                        {
                            var data = (from ad in db.AssociateDetails
                                        join ur in db.UserRoles on ad.EmployeeID equals ur.EmployeeID
                                        join r in db.Roles on ur.RoleId equals r.RoleId
                                        where ad.EmailId == context.UserName && ad.Password == context.Password
                                        select new AssociateRoles
                                        {
                                            AssociateId = ur.EmployeeID,
                                            AssociateName = ad.AssociateName,
                                            ContactNumber = ad.ContactNumber,
                                            EmailId = ad.EmailId,
                                            RoleId = ur.RoleId,
                                            RoleType = r.RoleType
                                        }).FirstOrDefault();

                            //var userRoles= db.UserRoles.ToList();
                            //var currentUser = userRoles.Where(u=>u.EmployeeID == )
                            //  var currentUser = user.Where(u => u.UserName == context.UserName && u.Password == context.Password).FirstOrDefault();
                            identity.AddClaim(new Claim("Role", data.RoleType));
                            identity.AddClaim(new Claim("Id", Convert.ToString(data.AssociateId)));
                            var props = new AuthenticationProperties(new Dictionary<string, string>{{"DisplayName", context.UserName},{"Role", data.RoleType}});
                            var ticket = new AuthenticationTicket(identity, props);
                            context.Validated(ticket);
                        }
                        else
                        {
                            context.SetError("invalid_grant", "Provided username and password is not matching, Please retry.");
                            context.Rejected();
                        }
                    }
                }
                else
                {
                    context.SetError("invalid_grant", "Provided username and password is not matching, Please retry.");
                    context.Rejected();
                }
                return;
            }
        }
    }
}



//}