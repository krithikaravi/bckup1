﻿using API.Controllers;
using API.Functions;
using Models.Models;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System;

namespace APITest
{
    [TestFixture]
    public class BusinessTests
    {
        VolunteerFunctions volunteerFunctions;
        List<UserRoles> userRoles;
       // MyDbContext dbContext;

        [SetUp]
        public void Setup()
        {
         userRoles=   GetUserRoles();
            volunteerFunctions = new VolunteerFunctions();
           // dbContext = new MyDbContext();
         //   AssociateDetails details;
            using (MyDbContext context = new MyDbContext())
            {
                var test=context.AssociateDetails.FirstOrDefault();
               // context.Open();
                // the rest
            }
            // details = dbContext.AssociateDetails.FirstOrDefault();
            //dbContext.
            // userRoles = SetupRoles();
        }

        public List<UserRoles> SetupRoles()
        {
            int _counter = new int();
            List<UserRoles> roles = volunteerFunctions.GetUserRoles().Select(x => new UserRoles {EmployeeID= x.EmployeeID,RoleId=x.RoleId,RoleType=x.RoleType}).ToList();

            foreach (var r in roles)
                r.Id = ++_counter;

            return roles;
        }

        //private List<UserRoles> GetUserRoles()
        //{
        //    var testProducts = new List<UserRoles>();
        //    testProducts.Add(new UserRoles { Id = 1, EmployeeID = 718432, RoleType ="Admin" });
        //    //testProducts.Add(new Product { Id = 2, Name = "Demo2", Price = 3.75M });
        //    //testProducts.Add(new Product { Id = 3, Name = "Demo3", Price = 16.99M });
        //    //testProducts.Add(new Product { Id = 4, Name = "Demo4", Price = 11.00M });

        //    return testProducts;
        //}

        [Test]
        public void Test1()
        {
            Assert.Pass();
        }

        [Ignore("not implemented")]
        public void ShouldReturnAllUserRoles1()
        {
            //var articles = eventDetailsController.GetEventDetails();

            //Assert.That(articles, Is.EqualTo(_randomArticles));
            //var userRoles = volunteerFunctions.GetUserRoles();
            //Assert.That(userRoles, Is.EqualTo(this.userRoles));

            using (MyDbContext context = new MyDbContext())
            {
                var test = context.UserRoles.FirstOrDefault();
                Assert.That(test,Is.AnyOf(this.userRoles));
                // context.Open();
                // the rest
            }

            
                //var dataContext = new MyEntities(connection);

                //CRUD operation1

                //CRUD operation2

                //CRUD operation3 ...
                //var userRoles = volunteerFunctions.GetUserRoles();
                //Assert.That(userRoles, Is.EqualTo(this.userRoles));
                // Assert.AreEqual(expected, actual);
              //  MyDbContext context = new MyDbContext();
              //  var response = context.AssociateDetails.Find(1);
                //Assert.IsTrue(response.Id>(1));
                //AssociateDetails associateDetails;
//Assert.IsTrue(response.Id>1);
               // Assert.AreEqual("Jignesh", employee.Name);


                //ar userRoles = volunteerFunctions.GetUserRoles();

        }
        [Test]
        public void GetWithoutParameter_ReturnsUsersList()
        {
            //Arrange
            // var mockService = new Mock<INewsAppService>();
            //mockService.Setup(service => service.GetAllArticles()).Returns(this.GetArticles());
            //var controller = new NewsAppController(mockService.Object);

            ////Act
            //var result = controller.Get();

            ////Assert
            //var actionResult = Assert.IsType<OkObjectResult>(result);
            //var model = Assert.IsAssignableFrom<IEnumerable<Article>>(actionResult.Value);
            var controller = new UserRolesController();

         //   var result = controller.GetUserRoles();
            List<UserRoles> roles = new List<UserRoles>();
            roles = controller.GetUserRoles().ToList();

            Assert.IsFalse(roles.Equals(this.userRoles));
            //Assert.arenotequal(roles, this.userRoles);
        }

        private List<UserRoles> GetUserRoles()
        {
             userRoles = new List<UserRoles>();
            userRoles.Add(new UserRoles {Id=2052,RoleId=1,EmployeeID=718432,RoleType="Admin"});
            userRoles.Add(new UserRoles { Id = 1, EmployeeID = 718432, RoleType = "Admin" });
            //userRoles.Add(new UserRoles { Id = 3, EmployeeID = 876543, RoleType = "Poc" });
            //userRoles.Add(new UserRoles { Id = 4, EmployeeID = 456382, RoleType = "User" });
            return userRoles;
        }

        [Test]
        public void GetAssocaiteDetails()
        {
            //ForumUser userToTest = new ForumUser();

            //TransactionScope transactionScope = new TransactionScope();

            //DataContextHandler.Context.AddToForumUser(userToTest);
            //DataContextHandler.Context.SaveChanges();

            //Assert.IsTrue(userToTest.UserID > 0);

            //var foundUser = (from user in DataContextHandler.Context.ForumUser
            //                 where user.UserID == userToTest.UserID
            //                 select user).Count();  //KABOOM Can't query since the 
            //                                        //transaction has the table locked.

            //Assert.IsTrue(foundUser == 1);

            //DataContextHandler.Context.Dispose();

            //var after = (from user in DataContextHandler.Context.ForumUser
            //             where user.UserID == userToTest.UserID
            //             select user).Count(); //KABOOM Can't query since the 
            //                                   //transaction has the table locked.

            //Assert.IsTrue(after == 0);

            MyDbContext context = new MyDbContext();
            var associateDetails = context.AssociateDetails.Find(1);
        }


    }
}
