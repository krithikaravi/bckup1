﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Models.Models
{
   public class Roles
    {
        [Key]
        [JsonProperty("roleId")]
        public int RoleId { get; set; }
        
        [Required]
        [JsonProperty("roleType")]
        public string RoleType { get; set; }

       
    }
}
