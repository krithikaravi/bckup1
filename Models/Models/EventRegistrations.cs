﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class EventRegistrations
    {
        [Key]
        //public int EventRegistrationId { get; set; }
        [JsonProperty("Id")]
        public int Id { get; set; }

        [JsonProperty("associateDetailsId")]
        public int AssociateDetailsId { get; set; }

        [JsonProperty("eventId")]
        public int EventId { get; set; }

        [JsonProperty("businessUnitId")]
        public int BusinessUnitId { get; set; }

        [JsonProperty("participationId")]
        public int ParticipationId { get; set; }

        [JsonProperty("employeeID")]
        public int EmployeeID { get; set; }

        [JsonProperty("employeeName")]
        public string EmployeeName { get; set; }

        [JsonProperty("contactNumber")]
        public long ContactNumber { get; set; }

        [JsonProperty("emailId")]
        public string EmailId { get; set; }

        [JsonProperty("waitingList")]
        public bool WaitingList { get; set; }

        //[JsonProperty("volunteerHours")]
        //public int VolunteerHours { get; set; }

        //[JsonProperty("travelHours")]
        //public int TravelHours { get; set; }

        //[JsonProperty("livesImpacted")]
        //public int LivesImpacted { get; set; }

        public EventDetails Event { get; set; }

        public AssociateDetails Associate { get; set; }

        [JsonProperty("businessUnit")]
        public BusinessUnit BusinessUnit { get; set; }

        [JsonProperty("transportTypeId")]
        public int TransportTypeId { get; set; }

        [JsonProperty("boardingPoint")]
        public string BoardingPoint { get; set; }

        [JsonProperty("dropPoint")]
        public string DropPoint { get; set; }
        //public Project ProjectDetails { get; set; }
        //public Council CouncilName { get; set; }
        public Participation ParticipationDetails { get; set; }
        //public EventStatus Status { get; set; }
        //public EventCategory Category { get; set; }
    }
}
