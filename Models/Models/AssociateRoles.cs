﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
  public class AssociateRoles
    {
        public int AssociateId { get; set; }
        public string AssociateName { get; set; }
        public long ContactNumber { get; set; }
        public string EmailId { get; set; }
        public int RoleId { get; set; }

        public string RoleType { get; set; }
    }
}
