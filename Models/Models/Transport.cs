﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
 public class Transport
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string TransportType { get; set; }

    }
}