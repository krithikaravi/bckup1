﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
  public  class BusinessUnit
    {
        [Key]
        [JsonProperty("id")]
        public int Id { get; set; }

        [Required]
        [JsonProperty("businessUnitName")]
        public string BusinessUnitName { get; set; }

        public ICollection<Project> Projects { get; set; }
    }
}
