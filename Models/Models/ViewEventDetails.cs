﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class ViewEventDetails
    {
        [JsonProperty("EventId")]
        public int EventId { get; set; }
        [JsonProperty("EventCode")]
        public string EventCode { get; set; }
        [JsonProperty("locationName")]
        public string LocationName { get; set; }
            [JsonProperty("beneficiaryName")]
            public string BeneficiaryName { get; set; }
        [JsonProperty("councilName")]
        public string CouncilName { get; set; }
        [JsonProperty("eventName")]
        public string EventName { get; set; }
        [JsonProperty("eventDescription")]
        public string EventDescription { get; set; }
        [JsonProperty("eventDate")]
        public DateTime EventDate { get; set; }
        [JsonProperty("employeeID")]
        public int EmployeeID { get; set; }
        [JsonProperty("employeeName")]
        public string EmployeeName { get; set; }
        [JsonProperty("businessUnit")]
        public string BusineeUnit { get; set; }

        [JsonProperty("volunteerHours")]
        public int VolunteerHours { get; set; }

        [JsonProperty("travelHours")]
        public int TravelHours { get; set; }

        [JsonProperty("livesImpacted")]
        public int LivesImpacted { get; set; }
        [JsonProperty("eventStatus")]
        public string EventStatus { get; set; }
        [JsonProperty("startTime")]
        public TimeSpan StartTime { get; set; }
        [JsonProperty("EndTime")]
        public TimeSpan EndTime { get; set; }
        [JsonProperty("Address")]
        public string Address { get; set; }
        [JsonProperty("transportTypeId")]
        public int TransportTypeId { get; set; }
        [JsonProperty("BoardingPoint")]
        public string BoardingPoint { get; set; }
        [JsonProperty("DropPoint")]
        public string DropPoint { get; set; }
        [JsonProperty("eventCategory")]
        public string EventCategory { get; set; }
        [JsonProperty("projectName")]
        public string ProjectName { get; set; }
        [JsonProperty("pocName")]
        public string PocName { get; set; }
        [JsonProperty("pocId")]
        public int PocId { get; set; }
        [JsonProperty("poccontactNumber")]
        public string PocContactNumber { get; set; }
        [JsonProperty("pocIds")]
        public string PocIds { get; set; }
        [JsonProperty("VolunteersRequired")]
        public int VolunteersRequired { get; set; }
        [JsonProperty("FavEvent")]
        public bool FavEvent { get; set; }
        [JsonProperty("totalVolunteerHours")]
        public int TotalVolunteerHours { get; set; }
        [JsonProperty("totalTravelHours")]
        public int TotalTravelHours { get; set; }
        [JsonProperty("overAllHours")]
        public int OverAllVolunteerHours { get; set; }
        [JsonProperty("totalVolunteerCount")]
        public int TotalVolunteerCount { get; set; }
        [JsonProperty("eventMonth")]
        public string EventMonth { get; set; }

    }
}
