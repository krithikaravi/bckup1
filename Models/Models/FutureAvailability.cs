﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
   public class FutureAvailability
    {
        [Key]
        public int Id { get; set; }
        [JsonProperty("availabilty")]
        public DateTime Availabilty { get; set; }
        [JsonProperty("location")]
        public string Location { get; set; }
        [JsonProperty("employeeID")]
        public int EmployeeID { get; set; }
        public AssociateDetails AssociateDetails { get; set; }
    }
}
