﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
   public class AssociateDetails
    {
        [Key]
        public int Id { get; set; }
        
       // [Required]
        [JsonProperty("employeeID")]
        public int EmployeeID { get; set; }
        //[Required]
        [JsonProperty("associateName")]
        public string AssociateName { get; set; }

        //[Required]
        [JsonProperty("contactNumber")]
        public long ContactNumber { get; set; }

       // [Required]
        [JsonProperty("emailId")]
        public string EmailId { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        //[JsonProperty("roleId")]
        //public int RoleId { get; set; }

        public UserRoles Users { get; set; }
    }
}
