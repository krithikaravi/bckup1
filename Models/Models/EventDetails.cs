﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Models
{
    public class EventDetails
    {
        [Key]
        [JsonProperty("eventId")]
        public int EventId { get; set; }

        [JsonProperty("eventCode")]
        public string EventCode { get; set; }

        //[Required]
        [JsonProperty("eventName")]
        public string EventName { get; set; }

        //[Required]
        [JsonProperty("eventDescription")]
        public string EventDescription { get; set; }

      //  [Required]
        [JsonProperty("eventDate")]
        public DateTime EventDate { get; set; }

        //[Required]
        [JsonProperty("startTime")]
        public TimeSpan StartTime { get; set; }

       // [Required]
        [JsonProperty("endTime")]
        public TimeSpan EndTime { get; set; }


        [JsonProperty("eventCategory")]
        public string EventCategory { get; set; }


        [JsonProperty("volunteersRequired")]
        public int VolunteersRequired { get; set; }

        [JsonProperty("transportTypeId")]
        public int TransportTypeId { get; set; }

        [JsonProperty("boardingPoint")]
        public string BoardingPoint { get; set; }

        [JsonProperty("dropPoint")]
        public string DropPoint { get; set; }

        [JsonProperty("pocId")]
        public int PocId { get; set; }

        //[JsonProperty("eventStatusId")]
        //public int EventStatusId { get; set; }

        [JsonProperty("eventStatus")]
        public string EventStatus { get; set; }

        [JsonProperty("favouriteEvent")]
        public bool FavouriteEvent { get; set; }

        [JsonProperty("projectName")]
        public string ProjectName { get; set; }

        [JsonProperty("locationName")]
        public string LocationName { get; set; }

        [JsonProperty("beneficiaryName")]
        public string BeneficiaryName { get; set; }

        [JsonProperty("councilName")]
        public string CouncilName { get; set; }

        [JsonProperty("transportType")]
        public Transport TransportType { get; set; }

        [JsonProperty("pocName")]
        public AssociateDetails PocName { get; set; }

        //[JsonProperty("status")]
        //public EventStatus Status { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("countLocation")]
        public int TopLocCount { get; set; }

        //[JsonProperty("registrations")]
        //public ICollection<EventRegistrations> Registrations { get; set; }

    }
}
